import unittest
import io
import sys
from Lista import Lista
from Nodo import Nodo
from Pedido import Pedido

numeroPedido1 = 4
descripcionPedido1 = "Descripcion 1"
costoPedido1 = 39

numeroPedido2 = 7
descripcionPedido2 = "Descripcion 2"
costoPedido2 = 49

numeroPedido3 = 12
descripcionPedido3 = "Descripcion 3"
costoPedido3 = 59

numeroPedido4 = 40
descripcionPedido4 = "Descripcion 4"
costoPedido4 = 19

class Pruebas(unittest.TestCase):
    def test_pruebas_pedido(self):
        pedido = Pedido(numeroPedido1, descripcionPedido1, costoPedido1)
        self.assertEqual(pedido.getCosto(), costoPedido1)
        self.assertEqual(pedido.getNumero(), numeroPedido1)
        self.assertEqual(pedido.getDescripcion(), descripcionPedido1)
        
        salidaImprimir = io.StringIO()
        sys.stdout = salidaImprimir
        pedido.mostrar()
        sys.stdout = sys.__stdout__
        salidaEsperada = "Numero pedido: 4\nDescripcion: Descripcion 1\nCosto: 39\n"
        self.assertEqual(salidaImprimir.getvalue(), salidaEsperada)

    def test_pruebas_nodo(self):
        nodo = Nodo()
        self.assertIsNone(nodo.getSiguiente())
        self.assertEqual(nodo.getPedido().getCosto(), 0)
        self.assertEqual(nodo.getPedido().getDescripcion(), "")
        self.assertEqual(nodo.getPedido().getNumero(), 0)

        nodoAux = Nodo()
        pedido1 = Pedido(numeroPedido1, descripcionPedido1, costoPedido1)
        pedido2 = Pedido(numeroPedido2, descripcionPedido2, costoPedido2)
        nodo.setPedido(pedido1)
        nodoAux.setPedido(pedido2)
        nodo.setSiguiente(nodoAux)

        self.assertEqual(nodo.getSiguiente(), nodoAux)
        self.assertIsNone(nodo.getSiguiente().getSiguiente())

        self.assertEqual(nodo.getPedido().getCosto(), costoPedido1)
        self.assertEqual(nodo.getPedido().getDescripcion(), descripcionPedido1)
        self.assertEqual(nodo.getPedido().getNumero(), numeroPedido1)

        self.assertEqual(nodo.getSiguiente().getPedido().getCosto(), costoPedido2)
        self.assertEqual(nodo.getSiguiente().getPedido().getDescripcion(), descripcionPedido2)
        self.assertEqual(nodo.getSiguiente().getPedido().getNumero(), numeroPedido2)

    def test_lista_agregar_al_final(self):
        listaEnlazada = Lista()
        listaEnlazada.agregarAlFinal(numeroPedido1, descripcionPedido1, costoPedido1)
        listaEnlazada.agregarAlFinal(numeroPedido2, descripcionPedido2, costoPedido2)
        listaEnlazada.agregarAlFinal(numeroPedido3, descripcionPedido3, costoPedido3)
        listaEnlazada.agregarAlFinal(numeroPedido4, descripcionPedido4, costoPedido4)

        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getPedido().getCosto(), costoPedido1)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getPedido().getDescripcion(), descripcionPedido1)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getPedido().getNumero(), numeroPedido1)

        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getPedido().getCosto(), costoPedido2)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getPedido().getDescripcion(), descripcionPedido2)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getPedido().getNumero(), numeroPedido2)

        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getSiguiente().getPedido().getCosto(), costoPedido3)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getSiguiente().getPedido().getDescripcion(), descripcionPedido3)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getSiguiente().getPedido().getNumero(), numeroPedido3)

        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getSiguiente().getSiguiente().getPedido().getCosto(), costoPedido4)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getSiguiente().getSiguiente().getPedido().getDescripcion(), descripcionPedido4)
        self.assertEqual(listaEnlazada.obtenerPrimerNodo().getSiguiente().getSiguiente().getSiguiente().getPedido().getNumero(), numeroPedido4)
        
    def test_lista_mostrar(self):
        listaEnlazada = Lista()
        listaEnlazada.agregarAlFinal(numeroPedido1, descripcionPedido1, costoPedido1)
        listaEnlazada.agregarAlFinal(numeroPedido2, descripcionPedido2, costoPedido2)
        listaEnlazada.agregarAlFinal(numeroPedido3, descripcionPedido3, costoPedido3)
        listaEnlazada.agregarAlFinal(numeroPedido4, descripcionPedido4, costoPedido4)

        salidaImprimir = io.StringIO()
        sys.stdout = salidaImprimir
        listaEnlazada.mostrarLista()
        sys.stdout = sys.__stdout__
        salidaEsperada = "Numero pedido: 4\nDescripcion: Descripcion 1\nCosto: 39\nNumero pedido: 7\nDescripcion: Descripcion 2\nCosto: 49\nNumero pedido: 12\nDescripcion: Descripcion 3\nCosto: 59\nNumero pedido: 40\nDescripcion: Descripcion 4\nCosto: 19\n"
        self.assertEqual(salidaImprimir.getvalue(), salidaEsperada)
