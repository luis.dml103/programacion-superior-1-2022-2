from Pedido import Pedido

class Nodo:
    def __init__(self):
        self.siguiente = None
        self.pedido = Pedido(0, "", 0)

    def getSiguiente(self):
        return self.siguiente

    def setSiguiente(self, nodo):
        self.siguiente = nodo

    def setPedido(self, pedido):
        self.pedido = pedido

    def getPedido(self):
        return self.pedido

