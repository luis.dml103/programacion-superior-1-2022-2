from Nodo import Nodo
from Pedido import Pedido

class Lista:
    def __init__(self):
        self.primerNodo = None

    def agregarAlFinal(self, numero, descripcion, costo):
        pedido = Pedido(numero, descripcion, costo)
        nodo = Nodo()
        nodo.setPedido(pedido)
        if self.primerNodo == None:
            self.primerNodo = nodo
        else:
            nodoAux = self.primerNodo
            while nodoAux.getSiguiente() != None:
                nodoAux = nodoAux.getSiguiente()
            nodoAux.setSiguiente(nodo)

    def mostrarLista(self):
        if self.primerNodo == None:
            print("La lista está vacía")
        else:
            nodoAux = self.primerNodo
            while nodoAux.getSiguiente() != None:
                nodoAux.getPedido().mostrar()
                nodoAux = nodoAux.getSiguiente()
            nodoAux.getPedido().mostrar()

    def obtenerPrimerNodo(self):
        return self.primerNodo