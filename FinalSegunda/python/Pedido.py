class Pedido:
    def __init__(self, numero, descripcion, costo):
        self.numero = numero
        self.descripcion = descripcion
        self.costo = costo

    def getNumero(self):
        return self.numero

    def getDescripcion(self):
        return self.descripcion

    def getCosto(self):
        return self.costo

    def mostrar(self):
        print("Numero pedido:", self.numero)
        print("Descripcion:", self.descripcion)
        print("Costo:", self.costo)