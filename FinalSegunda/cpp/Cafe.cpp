#include "Cafe.h"

Cafe::Cafe(float precio, string marcaRegistrada, int cantidadGramos) {
    this->precio = precio;
    this->tipo = cafe;
    this->marcaRegistrada = marcaRegistrada;
    this->cantidadGramos = cantidadGramos;
}
int Cafe::getCantidadGramos() {
    return cantidadGramos;
}
void Cafe::setCantidadGramos(int cantidadGramos) {
    this->cantidadGramos = cantidadGramos;
}
void Cafe::mostrar() {
    cout<< "Tipo Producto: Cafe"<<endl
        << "Precio: "<<precio<<endl
        << "Marca Registrada: "<<marcaRegistrada <<endl
        << "Cantidad gramos: "<<cantidadGramos<<endl;
}