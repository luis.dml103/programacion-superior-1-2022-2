#include "Manzana.h"

Manzana::Manzana(float precioManzana, string marcaRegistradaManzana, string colorManzana) {
    this->precio = precioManzana;
    this->marcaRegistrada = marcaRegistradaManzana;
    this->color = colorManzana;
    this->tipo = manzana;
}

string Manzana::getColor() {
    return this->color;
}
void Manzana::setColor(string color) {
    this->color = color;
}
void Manzana::mostrar() {
    cout<< "Tipo Producto: Manzana"<<endl
        << "Precio: "<<precio<<endl
        << "Marca Registrada: "<<marcaRegistrada <<endl
        << "Color: "<<color<<endl;
}