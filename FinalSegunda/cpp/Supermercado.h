#include <iostream>
#include <vector>
#include "Producto.h"

class Supermercado {
    private:
        vector<Producto*> listaCarritoCompras;
        vector<Producto*> productos;
    public:
        Supermercado();
        void vaciarCarrito();
        vector<Producto*> getListaCarritoCompras();
        void registrarProducto(Productos tipo);
        void mostrarDetallesProducto(Productos tipo);
        void agregarProductoAlCarritoDeCompras(Productos tipo);
        void realizarVentaCarritoDeCompras();
};