#pragma once
#include <iostream>
#include "EnumProductos.cpp"
using namespace std;

class Producto {
    protected:
        Productos tipo;
        float precio;
        string marcaRegistrada;
    public:
        Productos getTipo();
        string getMarcaRegistrada();
        float getPrecio();
        void setTipo(Productos tipoProducto);
        void setMarcaRegistrada(string marcaRegistrada);
        void setPrecio(float precio);
        virtual void mostrar() = 0;
};