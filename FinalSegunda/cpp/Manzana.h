#pragma once
#include <iostream>
#include "Producto.h"

using namespace std;

class Manzana: public Producto {
    private:
    string color;
    public:
        Manzana(float precioManzana, string marcaRegistradaManzana, string colorManzana);
        string getColor();
        void setColor(string color);
        void mostrar();
};