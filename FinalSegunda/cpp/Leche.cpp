#include "Leche.h"

Leche::Leche(float precioLeche, string marcaRegistrada, float porcentajeGrasa) {
    this->precio = precioLeche;
    this->marcaRegistrada = marcaRegistrada;
    this->porcentajeGrasa = porcentajeGrasa;
    this->tipo = leche;
}
float Leche::getPorcentajeGrasa() {
    return porcentajeGrasa;
}
void Leche::setPorcentajeGrasa(float porcentajeGrasa) {
    this->porcentajeGrasa = porcentajeGrasa;
}
void Leche::mostrar() {
    cout<< "Tipo Producto: Leche"<<endl
        << "Precio: "<<precio<<endl
        << "Marca Registrada: "<<marcaRegistrada <<endl
        << "Porcentaje grasa: "<<porcentajeGrasa<<endl;
}