#pragma once
#include <iostream>
#include "Producto.h"

using namespace std;

class Cafe: public Producto {
    private:
    int cantidadGramos;
    public:
        Cafe(float precio, string marcaRegistrada, int cantidadGramos);
        int getCantidadGramos();
        void setCantidadGramos(int cantidadGramos);
        void mostrar();
};