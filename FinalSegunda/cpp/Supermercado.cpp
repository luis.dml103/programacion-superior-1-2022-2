#include "Supermercado.h"
#include "Cafe.h"
#include "Leche.h"
#include "Manzana.h"

Supermercado::Supermercado() {
    this->productos.push_back(new Cafe(30, "Monaco", 300));
    this->productos.push_back(new Manzana(10, "San Benito", "Verde"));
    this->productos.push_back(new Leche(6, "PIL", 3));
}

void Supermercado::vaciarCarrito() {
    listaCarritoCompras.clear();
}

vector<Producto*> Supermercado::getListaCarritoCompras() {
    return this->listaCarritoCompras;
}

void Supermercado::registrarProducto(Productos tipo) {
    
}

void Supermercado::mostrarDetallesProducto(Productos tipo) {
    for (int i = 0; i<productos.size(); i++) {
        if (productos[i]->getTipo() == tipo) {
            productos[i]->mostrar();
        }
    }
}

void Supermercado::agregarProductoAlCarritoDeCompras(Productos tipo) {
    for (int i = 0; i<productos.size(); i++) {
        if (productos[i]->getTipo() == tipo) {
            listaCarritoCompras.push_back(productos[i]);
        }
    }
}

void Supermercado::realizarVentaCarritoDeCompras() {
    float precioTotal = 0;
    for (int i = 0; i<listaCarritoCompras.size(); i++) {
        listaCarritoCompras[i]->mostrar();
        precioTotal += listaCarritoCompras[i]->getPrecio();
    }
    cout<<"Precio total: "<<precioTotal<<endl;
    this->vaciarCarrito();
}