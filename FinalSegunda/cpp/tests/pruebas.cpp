#include <gtest/gtest.h>

#include "../EnumProductos.cpp"
#include "../Manzana.cpp"
#include "../Cafe.cpp"
#include "../Leche.cpp"
#include "../Producto.cpp"
#include "../Supermercado.cpp"

float precioManzana = 10;
string colorManzana = "Verde";
string marcaRegistradaManzana = "San Benito";

float precioCafe = 30;
int cantidadGramosCafe = 300;
string marcaRegistradaCafe = "Monaco";

float precioLeche = 6;
float porcentajeGrasa = 3;
string marcaRegistradaLeche = "PIL";

TEST(Producto, pruebasGettersSettersManzana) {
    Producto* producto = new Manzana(precioManzana, marcaRegistradaManzana, colorManzana);

    ASSERT_EQ(producto->getMarcaRegistrada(), marcaRegistradaManzana);
    ASSERT_EQ(producto->getPrecio(), precioManzana);
    ASSERT_EQ(producto->getTipo(), manzana);
}

TEST(Producto, pruebasGettersSettersCafe) {
    Producto* producto = new Cafe(precioCafe, marcaRegistradaCafe, cantidadGramosCafe);

    ASSERT_EQ(producto->getMarcaRegistrada(), marcaRegistradaCafe);
    ASSERT_EQ(producto->getPrecio(), precioCafe);
    ASSERT_EQ(producto->getTipo(), cafe);
}

TEST(Producto, pruebasGettersSettersLeche) {
    Producto* producto = new Leche(precioLeche, marcaRegistradaLeche, porcentajeGrasa);

    ASSERT_EQ(producto->getMarcaRegistrada(), marcaRegistradaLeche);
    ASSERT_EQ(producto->getPrecio(), precioLeche);
    ASSERT_EQ(producto->getTipo(), leche);
}

TEST(Producto, pruebaMostrarManzana) {
    Producto* producto = new Manzana(precioManzana, marcaRegistradaManzana, colorManzana);

    testing::internal::CaptureStdout();
    producto->mostrar();
    std::string datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Tipo Producto: Manzana\nPrecio: 10\nMarca Registrada: San Benito\nColor: Verde\n");
}

TEST(Producto, pruebaMostrarLeche) {
    Producto* producto = new Leche(precioLeche, marcaRegistradaLeche, porcentajeGrasa);

    testing::internal::CaptureStdout();
    producto->mostrar();
    std::string datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Tipo Producto: Leche\nPrecio: 6\nMarca Registrada: PIL\nPorcentaje grasa: 3\n");
}

TEST(Producto, pruebaMostrarCafe) {
    Producto* producto = new Cafe(precioCafe, marcaRegistradaCafe, cantidadGramosCafe);

    testing::internal::CaptureStdout();
    producto->mostrar();
    std::string datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Tipo Producto: Cafe\nPrecio: 30\nMarca Registrada: Monaco\nCantidad gramos: 300\n");
}

TEST(Supermercado, mostrarDetallesProducto) {
    Supermercado supermercado;
    //Manzana
    testing::internal::CaptureStdout();
    supermercado.mostrarDetallesProducto(manzana);
    std::string datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Tipo Producto: Manzana\nPrecio: 10\nMarca Registrada: San Benito\nColor: Verde\n");
    //Cafe
    testing::internal::CaptureStdout();
    supermercado.mostrarDetallesProducto(cafe);
    datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Tipo Producto: Cafe\nPrecio: 30\nMarca Registrada: Monaco\nCantidad gramos: 300\n");
    //Leche
    testing::internal::CaptureStdout();
    supermercado.mostrarDetallesProducto(leche);
    datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Tipo Producto: Leche\nPrecio: 6\nMarca Registrada: PIL\nPorcentaje grasa: 3\n");
}

TEST(Supermercado, agregarProductoAlCarritoDeCompras) {
    Supermercado supermercado;
    //Agregar manzana
    supermercado.agregarProductoAlCarritoDeCompras(manzana);
    
    ASSERT_EQ(supermercado.getListaCarritoCompras()[0]->getTipo(), manzana);
    supermercado.agregarProductoAlCarritoDeCompras(manzana);
    ASSERT_EQ(supermercado.getListaCarritoCompras()[1]->getTipo(), manzana);
    supermercado.agregarProductoAlCarritoDeCompras(cafe);
    ASSERT_EQ(supermercado.getListaCarritoCompras()[2]->getTipo(), cafe);
    supermercado.agregarProductoAlCarritoDeCompras(leche);
    ASSERT_EQ(supermercado.getListaCarritoCompras()[3]->getTipo(), leche);
}

TEST(Supermercado, ventaCarritoDeCompras) {
    Supermercado supermercado;
    //Agregar manzana
    supermercado.agregarProductoAlCarritoDeCompras(manzana);
    supermercado.agregarProductoAlCarritoDeCompras(manzana);
    supermercado.agregarProductoAlCarritoDeCompras(cafe);
    supermercado.agregarProductoAlCarritoDeCompras(leche);

    testing::internal::CaptureStdout();
    supermercado.realizarVentaCarritoDeCompras();
    string datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Tipo Producto: Manzana\nPrecio: 10\nMarca Registrada: San Benito\nColor: Verde\nTipo Producto: Manzana\nPrecio: 10\nMarca Registrada: San Benito\nColor: Verde\nTipo Producto: Cafe\nPrecio: 30\nMarca Registrada: Monaco\nCantidad gramos: 300\nTipo Producto: Leche\nPrecio: 6\nMarca Registrada: PIL\nPorcentaje grasa: 3\nPrecio total: 56\n");

    //Verificar que el carrito de compras este vacio despues de la venta
    ASSERT_TRUE(supermercado.getListaCarritoCompras().empty());
}