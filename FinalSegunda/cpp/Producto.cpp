#include "Producto.h"

Productos Producto::getTipo() {
    return this->tipo;
}
string Producto::getMarcaRegistrada() {
    return this->marcaRegistrada;
}
float Producto::getPrecio() {
    return this->precio;
}
void Producto::setTipo(Productos tipoProducto) {
    this->tipo = tipoProducto;
}
void Producto::setMarcaRegistrada(string marcaRegistrada) {
    this->marcaRegistrada = marcaRegistrada;
}
void Producto::setPrecio(float precio) {
    this->precio = precio;
}