#pragma once
#include <iostream>
#include "Producto.h"

using namespace std;

class Leche: public Producto {
    private:
        float porcentajeGrasa;
    public:
        Leche(float precioLeche, string marcaRegistradaManzana, float porcentajeGrasa);
        float getPorcentajeGrasa();
        void setPorcentajeGrasa(float porcentajeGrasa);
        void mostrar();
};