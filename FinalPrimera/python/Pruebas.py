from inspect import isabstract
import unittest
import io
import sys

from Empleado import Empleado
from EmpleadoTiempoCompleto import EmpleadoTiempoCompleto
from EmpleadoTiempoHorario import EmpleadoTiempoHorario
from PagoPlanilla import PagoPlanilla

nombrePrueba1 = "Andrea"
apellidoPrueba1 = "Perez"
salarioPrueba1 = 2000

nombrePrueba2 = "Gabriel"
apellidoPrueba2 = "Sanchez"
pagoPorHoraPrueba2 = 50
horasTrabajadasPrueba2 = 20

nombrePrueba3 = "Pablo"
apellidoPrueba3 = "Santillana"
salarioPrueba3 = 1900

nombrePrueba4 = "Hector"
apellidoPrueba4 = "Nogales"
pagoPorHoraPrueba4 = 45
horasTrabajadasPrueba4 = 15

class Pruebas(unittest.TestCase):
    def test_empleado_tiempo_completo(self):
        empleadoTiempoCompleto = EmpleadoTiempoCompleto(nombrePrueba1, apellidoPrueba1, salarioPrueba1)
        
        self.assertEqual(issubclass(empleadoTiempoCompleto.__class__, Empleado), True)
        
        self.assertEqual(empleadoTiempoCompleto.getNombreCompleto(), nombrePrueba1 + " " + apellidoPrueba1)
        self.assertEqual(empleadoTiempoCompleto.getSalario(), salarioPrueba1)

    def test_empleado_tiempo_horario(self):
        empleadoTiempoHorario = EmpleadoTiempoHorario(nombrePrueba2, apellidoPrueba2, horasTrabajadasPrueba2, pagoPorHoraPrueba2)
        
        self.assertEqual(issubclass(empleadoTiempoHorario.__class__, Empleado), True)
        
        self.assertEqual(empleadoTiempoHorario.getNombreCompleto(), nombrePrueba2 + " " + apellidoPrueba2)
        self.assertEqual(empleadoTiempoHorario.getSalario(), pagoPorHoraPrueba2 * horasTrabajadasPrueba2)

    def test_empleado(self):
        self.assertEqual(isabstract(Empleado), True)
        
    def test_planilla_pago(self):
        planillaDePago = PagoPlanilla()
        planillaDePago.registrarEmpleadoTiempoCompleto(nombrePrueba1, apellidoPrueba1, salarioPrueba1)
        planillaDePago.registrarEmpleadoTiempoHorario(nombrePrueba2, apellidoPrueba2, horasTrabajadasPrueba2, pagoPorHoraPrueba2)
        planillaDePago.registrarEmpleadoTiempoCompleto(nombrePrueba3, apellidoPrueba3, salarioPrueba3)
        planillaDePago.registrarEmpleadoTiempoHorario(nombrePrueba4, apellidoPrueba4, horasTrabajadasPrueba4, pagoPorHoraPrueba4)

        salidaImprimir = io.StringIO()
        sys.stdout = salidaImprimir
        planillaDePago.imprimirPlanilla()
        sys.stdout = sys.__stdout__
        salidaEsperada = "Nombre completo: " +  nombrePrueba1 + " " + apellidoPrueba1 + " - Salario: " + str(salarioPrueba1)
        salidaEsperada += "\nNombre completo: " +  nombrePrueba2 + " " + apellidoPrueba2 + " - Salario: " + str(pagoPorHoraPrueba2 * horasTrabajadasPrueba2)
        salidaEsperada += "\nNombre completo: " +  nombrePrueba3 + " " + apellidoPrueba3 + " - Salario: " + str(salarioPrueba3)
        salidaEsperada += "\nNombre completo: " +  nombrePrueba4 + " " + apellidoPrueba4 + " - Salario: " + str(pagoPorHoraPrueba4 * horasTrabajadasPrueba4) + "\n"
        self.assertEqual(salidaImprimir.getvalue(), salidaEsperada)