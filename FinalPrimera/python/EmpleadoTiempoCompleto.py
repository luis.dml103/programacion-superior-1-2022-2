from Empleado import Empleado

class EmpleadoTiempoCompleto(Empleado):

    def __init__(self, nombre, apellido, salario):
        super().__init__(nombre, apellido)
        self.__salario = salario

    def getSalario(self):
        return self.__salario