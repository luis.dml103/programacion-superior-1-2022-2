from Empleado import Empleado

class EmpleadoTiempoHorario(Empleado):

    def __init__(self, nombre, apellido, horasTrabajadas, pagoHora):
        super().__init__(nombre, apellido)
        self.__horas_trabajadas = horasTrabajadas
        self.__pago_por_hora = pagoHora

    def getSalario(self):
        return self.__horas_trabajadas * self.__pago_por_hora