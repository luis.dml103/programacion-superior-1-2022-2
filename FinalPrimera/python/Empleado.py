from abc import ABC, abstractmethod


class Empleado(ABC):
    def __init__(self, nombre, apellido):
        self._nombre = nombre
        self._apellido = apellido

    def getNombreCompleto(self):
        return self._nombre + " " + self._apellido

    @abstractmethod
    def getSalario(self):
        pass
