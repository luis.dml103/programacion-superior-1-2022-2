from typing import List
from Empleado import Empleado
from EmpleadoTiempoCompleto import EmpleadoTiempoCompleto
from EmpleadoTiempoHorario import EmpleadoTiempoHorario

class PagoPlanilla:
    def __init__(self):
        self.__empleados = []

    def registrarEmpleadoTiempoHorario(self, nombre, apellido, horasTrabajadas, pagoHora):
        empleadoTiempoHorario = EmpleadoTiempoHorario(nombre, apellido, horasTrabajadas, pagoHora)
        self.__empleados.append(empleadoTiempoHorario)

    def registrarEmpleadoTiempoCompleto(self, nombre, apellido, salario):
        empleadoTiempoCompleto = EmpleadoTiempoCompleto(nombre, apellido, salario)
        self.__empleados.append(empleadoTiempoCompleto)

    def imprimirPlanilla(self):
        for empleado in self.__empleados:
            print("Nombre completo:", empleado.getNombreCompleto(), "- Salario:", empleado.getSalario())