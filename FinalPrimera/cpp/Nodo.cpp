#include "Nodo.h"
Nodo::Nodo() {
    this->siguiente = NULL;
}

void Nodo::setSiguiente(Nodo* nodo) {
    this->siguiente = nodo;
}

Nodo* Nodo::getSiguiente() {
    return this->siguiente;
}

void Nodo::setPedido(Pedido pedido) {
    this->pedido = pedido;
}

Pedido Nodo::getPedido() {
    return this->pedido;
}