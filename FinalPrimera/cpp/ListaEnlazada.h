#pragma once
#include "Nodo.h"

class ListaEnlazada {
    private:
        Nodo* primerNodo;

    public:
        ListaEnlazada();
        void agregarAlFinal(int numeroPedido, string descripcionPedido, float costoPedido);
        void mostrarLista();
        void mostrarListaPorPrecio();
        Nodo* obtenerPrimerNodo();
};