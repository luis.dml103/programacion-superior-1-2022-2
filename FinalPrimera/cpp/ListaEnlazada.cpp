#include "ListaEnlazada.h"

ListaEnlazada::ListaEnlazada() {
    this->primerNodo = NULL;
}

void ListaEnlazada::agregarAlFinal(int numeroPedido, string descripcionPedido, float costoPedido) {
    Nodo* aux = new Nodo();
    Pedido pedido(numeroPedido, descripcionPedido, costoPedido);
    aux->setPedido(pedido);
    if (this->primerNodo == NULL) {
        this->primerNodo = aux;
    } else {
        Nodo* aux2 = this->primerNodo;
        while (aux2->getSiguiente() != NULL) {
            aux2 = aux2->getSiguiente();
        }
        aux2->setSiguiente(aux);
    }
}

void ListaEnlazada::mostrarLista() {
    if (this->primerNodo == NULL) {
        cout <<"Lista vacia."<<endl;
        return;
    }

    Nodo* aux = this->primerNodo;
    while (aux->getSiguiente() != NULL) {
        aux->getPedido().mostrar();
        aux = aux->getSiguiente();
    }
    aux->getPedido().mostrar();
}

Nodo* ListaEnlazada::obtenerPrimerNodo() {
    return primerNodo;
}