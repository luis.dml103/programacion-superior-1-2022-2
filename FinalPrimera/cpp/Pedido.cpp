#include "Pedido.h"

Pedido::Pedido() {
    this->numero = 0;
    this->descripcion = "";
    this->costo = 0;
}

Pedido::Pedido(int numero, string descripcion, float costo) {
    this->numero = numero;
    this->descripcion = descripcion;
    this->costo = costo;
}

int Pedido::getNumero() {
    return this->numero;
}

string Pedido::getDescripcion() {
    return this->descripcion;
}

float Pedido::getCosto() {
    return this->costo;
}

void Pedido::mostrar() {
    cout<< "Numero pedido: " << numero <<endl
        << "Descripcion: " << descripcion <<endl
        << "Costo: " << costo<<endl;
}