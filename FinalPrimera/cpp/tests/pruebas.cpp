#include <gtest/gtest.h>
#include <iostream>
#include "../Nodo.cpp"
#include "../Pedido.cpp"
#include "../ListaEnlazada.cpp"

int numeroPedido1 = 4;
string descripcionPedido1 = "Descripcion 1";
float costoPedido1 = 39;

int numeroPedido2 = 7;
string descripcionPedido2 = "Descripcion 2";
float costoPedido2 = 49;

int numeroPedido3 = 12;
string descripcionPedido3 = "Descripcion 3";
float costoPedido3 = 59;

int numeroPedido4 = 40;
string descripcionPedido4 = "Descripcion 4";
float costoPedido4 = 19;


TEST(Pedido, pruebasPedido) {
    Pedido pedido;
    ASSERT_EQ(pedido.getCosto(), 0);
    ASSERT_EQ(pedido.getNumero(), 0);
    ASSERT_EQ(pedido.getDescripcion(), "");

    Pedido pedido1(numeroPedido1, descripcionPedido1, costoPedido1);
    ASSERT_EQ(pedido1.getCosto(), costoPedido1);
    ASSERT_EQ(pedido1.getNumero(), numeroPedido1);
    ASSERT_EQ(pedido1.getDescripcion(), descripcionPedido1);

    testing::internal::CaptureStdout();
    pedido1.mostrar();
    std::string datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Numero pedido: 4\nDescripcion: Descripcion 1\nCosto: 39\n");
}


TEST(Nodo, pruebasNodo) {
    Nodo* nodo = new Nodo();
    
    ASSERT_TRUE(nodo->getSiguiente() == NULL);
    ASSERT_EQ(nodo->getPedido().getCosto(), 0);
    ASSERT_EQ(nodo->getPedido().getDescripcion(), "");
    ASSERT_EQ(nodo->getPedido().getNumero(), 0);

    Nodo* nodoAux = new Nodo();
    Pedido pedido1(numeroPedido1, descripcionPedido1, costoPedido1);
    Pedido pedido2(numeroPedido2, descripcionPedido2, costoPedido2);
    nodo->setPedido(pedido1);
    nodoAux->setPedido(pedido2);
    nodo->setSiguiente(nodoAux);

    ASSERT_EQ(nodo->getSiguiente(), nodoAux);
    ASSERT_TRUE(nodo->getSiguiente()->getSiguiente() == NULL);

    ASSERT_EQ(nodo->getPedido().getCosto(), costoPedido1);
    ASSERT_EQ(nodo->getPedido().getDescripcion(), descripcionPedido1);
    ASSERT_EQ(nodo->getPedido().getNumero(), numeroPedido1);

    ASSERT_EQ(nodo->getSiguiente()->getPedido().getCosto(), costoPedido2);
    ASSERT_EQ(nodo->getSiguiente()->getPedido().getDescripcion(), descripcionPedido2);
    ASSERT_EQ(nodo->getSiguiente()->getPedido().getNumero(), numeroPedido2);

    delete nodo, nodoAux;
}

TEST(ListaEnlazada, agregarAlFinal) {
    ListaEnlazada listaEnlazada;
    listaEnlazada.agregarAlFinal(numeroPedido1, descripcionPedido1, costoPedido1);
    listaEnlazada.agregarAlFinal(numeroPedido2, descripcionPedido2, costoPedido2);
    listaEnlazada.agregarAlFinal(numeroPedido3, descripcionPedido3, costoPedido3);
    listaEnlazada.agregarAlFinal(numeroPedido4, descripcionPedido4, costoPedido4);

    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getPedido().getCosto(), costoPedido1);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getPedido().getDescripcion(), descripcionPedido1);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getPedido().getNumero(), numeroPedido1);

    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getPedido().getCosto(), costoPedido2);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getPedido().getDescripcion(), descripcionPedido2);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getPedido().getNumero(), numeroPedido2);

    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getSiguiente()->getPedido().getCosto(), costoPedido3);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getSiguiente()->getPedido().getDescripcion(), descripcionPedido3);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getSiguiente()->getPedido().getNumero(), numeroPedido3);

    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getSiguiente()->getSiguiente()->getPedido().getCosto(), costoPedido4);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getSiguiente()->getSiguiente()->getPedido().getDescripcion(), descripcionPedido4);
    ASSERT_EQ(listaEnlazada.obtenerPrimerNodo()->getSiguiente()->getSiguiente()->getSiguiente()->getPedido().getNumero(), numeroPedido4);
}

TEST(ListaEnlazada, mostrar) {
    ListaEnlazada listaEnlazada;
    listaEnlazada.agregarAlFinal(numeroPedido1, descripcionPedido1, costoPedido1);
    listaEnlazada.agregarAlFinal(numeroPedido2, descripcionPedido2, costoPedido2);
    listaEnlazada.agregarAlFinal(numeroPedido3, descripcionPedido3, costoPedido3);
    listaEnlazada.agregarAlFinal(numeroPedido4, descripcionPedido4, costoPedido4);

    testing::internal::CaptureStdout();
    listaEnlazada.mostrarLista();
    std::string datos = testing::internal::GetCapturedStdout();
    ASSERT_EQ(datos, "Numero pedido: 4\nDescripcion: Descripcion 1\nCosto: 39\nNumero pedido: 7\nDescripcion: Descripcion 2\nCosto: 49\nNumero pedido: 12\nDescripcion: Descripcion 3\nCosto: 59\nNumero pedido: 40\nDescripcion: Descripcion 4\nCosto: 19\n");
}
