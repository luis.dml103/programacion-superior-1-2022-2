#pragma once
#include <iostream>
#include "Pedido.h"

class Nodo {
    private:
        Nodo* siguiente;
        Pedido pedido;
    public:
        Nodo();
        void setSiguiente(Nodo* nodo);
        Nodo* getSiguiente();
        void setPedido(Pedido);
        Pedido getPedido();
};