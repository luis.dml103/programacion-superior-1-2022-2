#pragma once
#include<iostream>
using namespace std;

class Pedido {
    private:
        string descripcion;
        float costo;
        int numero;
    public:
        Pedido();
        Pedido(int numero, string descripcion, float costo);
        int getNumero();
        string getDescripcion();
        float getCosto();
        void mostrar();
};